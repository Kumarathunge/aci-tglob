ACi-TGlob_V1.0: A Global dataset of photosynthetic CO2 response curves of terrestrial plants. 

Global vegetation models (GVMs) are one of the major tools used to predict global forest carbon balance in future warmed climates. 
GVMs simulate land carbon exchange processes through the mechanistic (and/or empirical) representation of the underline processes 
such as photosynthesis and respiration and their responses to environmental drivers.  Photosynthesis is one of the key component in 
GVMs therefore, robust representation of photosynthesis and its responses to environment is important in predicting future global carbon budget. 
Robust parameterization of photosynthesis in GVMs require data spanning geographically to represent species� adaptive responses to their climate of origin, 
as well as temporally to represent acclimation responses to short-term changes in growth environment. Here we present 1) raw data to parameterize key 
photosynthetic biochemical parameters; maximum rate of ribulose-1,5-bisphosphate carboxylase-oxygenase (Rubisco) activity (Vcmax), potential rate of 
electron transport (Jmax) and rate of triose phosphate export from the chloroplast (TPU), 2) estimated Vcmax, Jmax and TPU parameter values for 141 
plant species from 42 experiments conducted around the world.  The database includes observations for most plant functional types (PFTs) from tropical 
rain forests to boreal forests and data from Arctic tundra. Site latitude ranged from 42�48' S to 71�16' N and mean annual growing season temperature ranged from 3 to 30�C. 
The dataset contains raw photosynthetic CO2 response curves (ACi curves) measured at different leaf temperature temperatures, 
and estimated parameters (Vcmax, Jmax and TPU) and their temperature response parameters. It includes data from different types of experiments 
(e.g. temperature warming experiments, CO2 enrichment experiments, drought experiments) allowing examination of parameter responses to environmental drivers.
We expect that the data presented here will be useful for better parameterization and evaluation of GVMs thereby improve the predictions on future forest carbon balance. 

