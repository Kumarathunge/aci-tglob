
File information

ACi-TGlob_V1.0.csv
The dataset containing individual ACi curves. The column �Curve_Id� contains a unique identification number for each ACi curve in the dataset that link raw ACi data to the fitted parameters. Variable description is given in ACi-TGlob_V1.0_metadata.csv.

ACi-TGlob_V1.0_metadata.csv.
Variable description and units of each column of ACi-TGlob_V1.0.csv

PPC-TGlob_V1.0.csv
The dataset containing fitted Vcmax, Jmax, TPU and Rd for each ACi curve in the ACi-TGlob_V1.0 dataset. The column �Curve_Id� contains a unique identification number corresponding for each raw ACi curve of the ACi-TGlob_V1.0 dataset. Variable description is given in PPC-TGlob_V1.0_metadata.csv

PPC-TGlob_V1.0_metadata.csv
Variable description and units of each column of PPC-TGlob_V1.0.csv
