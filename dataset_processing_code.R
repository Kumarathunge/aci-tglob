
#------------------------------------------------------------------------------------------------------------
aci_dat<-read.csv("Data/ACi-TGlob_V1.0.csv",stringsAsFactors = FALSE)


# correct temperature treatment

aci_dat$Temp_Treatment[which(aci_dat$Temp_Treatment==1)]<-"18C"
aci_dat$Temp_Treatment[which(aci_dat$Temp_Treatment==4)]<-"28.5C"
aci_dat$Temp_Treatment[which(aci_dat$Temp_Treatment==6)]<-"35.5C"
aci_dat$Temp_Treatment[which(aci_dat$Temp_Treatment=="15")]<-"15C"
aci_dat$Temp_Treatment[which(aci_dat$Temp_Treatment=="20")]<-"20C"
aci_dat$Temp_Treatment[which(aci_dat$Temp_Treatment=="25")]<-"25C"
aci_dat$Temp_Treatment[which(aci_dat$Temp_Treatment=="30")]<-"30C"
aci_dat$Temp_Treatment[which(aci_dat$Temp_Treatment=="35")]<-"35C"
aci_dat$Temp_Treatment[which(aci_dat$Temp_Treatment=="No_Temperature_Treatments")]<-NA
aci_dat$Temp_Treatment[which(aci_dat$Temp_Treatment=="C")]<-"17C"
aci_dat$Temp_Treatment[which(aci_dat$Temp_Treatment=="W")]<-"23C"

#------------------------------------------------------------------------------------------------------------
unique(aci_dat$Species)


aci_dat$Species[which(aci_dat$Species=="Picea mariana ")]<-"Picea mariana"

aci_dat$Species[which(aci_dat$Species=="Corymbia calophylla_CD")]<-"Corymbia calophylla (prov Cape Richie)"
aci_dat$Species[which(aci_dat$Species=="Corymbia calophylla_CW")]<-"Corymbia calophylla (prov Boorara)"
aci_dat$Species[which(aci_dat$Species=="Corymbia calophylla_WD")]<-"Corymbia calophylla (prov Mogumber)"
aci_dat$Species[which(aci_dat$Species=="Corymbia calophylla_WW")]<-"Corymbia calophylla (prov Gingin)"

aci_dat$Species[which(aci_dat$Species=="Eucalyptus tereticornis_C")]<-"Eucalyptus tereticornis (prov temperate)"
aci_dat$Species[which(aci_dat$Species=="Eucalyptus tereticornis_W")]<-"Eucalyptus tereticornis (prov tropical)"

aci_dat$Species[which(aci_dat$Species=="Pouteria anomola")]<-"Pouteria anomala"

aci_dat$Species[which(aci_dat$Species=="Landes")]<-"Pinus pinaster (prov Landes)"
aci_dat$Species[which(aci_dat$Species=="Tamjoute")]<-"Pinus pinaster (prov Tamjoute)"

aci_dat$Species[which(aci_dat$Species=="tabanuco")]<-"Dacryodes excels"

aci_dat$Species[which(aci_dat$Species=="C.g.")]<-"Carapa grandiflora"
aci_dat$Species[which(aci_dat$Species=="C.s.")]<-"Cedrela serrata"
aci_dat$Species[which(aci_dat$Species=="E.e.")]<-"Entandrophragma excelsum"
aci_dat$Species[which(aci_dat$Species=="E.i.")]<-"Eucalyptus maidenii"
aci_dat$Species[which(aci_dat$Species=="E.m.")]<-"Eucalyptus microcorys"
aci_dat$Species[which(aci_dat$Species=="H.a.")]<-"Hagenia abyssinica"

aci_dat$Species[which(aci_dat$Species=="Eucalyptus tereticornis_Co")]<-"Eucalyptus tereticornis (prov temperate)"

aci_dat$Species[which(aci_dat$Species== "Pinus sylvestris_haguenau" )]<- "Pinus sylvestris (prov Haguenau)" 
aci_dat$Species[which(aci_dat$Species=="Pinus sylvestris_suprasl" )]<-"Pinus sylvestris (prov Suprasl)" 
aci_dat$Species[which(aci_dat$Species=="Pinus sylvestris_spala")]<-"Pinus sylvestris (peov Spala)"
aci_dat$Species[which(aci_dat$Species=="Pinus sylvestris_sumpberget")]<-"Pinus sylvestris prov(Sumpberget)"
aci_dat$Species[which(aci_dat$Species=="Pinus sylvestris_ussr")]<-"Pinus sylvestris (prov Ussr)"
aci_dat$Species[which(aci_dat$Species=="Pinus sylvestris_prusacka rijeka")]<-"Pinus sylvestris (prov Prusacka Rijeka)"


aci_dat$species<-NULL 

# remove three bad curves
aci_dat<-subset(aci_dat,!aci_dat$Curve_number %in% c(2737,3096,3335))


write.csv(aci_dat,"Data/ACi-TGlob_V1.0.csv")


unique(aci_dat$Temp_Treatment)
unique(aci_dat$CO2_Treatment)
unique(aci_dat$Dataset)
unique(aci_dat$Species)

